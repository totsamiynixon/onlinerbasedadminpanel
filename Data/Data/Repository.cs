﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Context;
using Core.Data;
using EFModels.Models;

namespace Data.Data
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IApplicationContext _context;
        private readonly IDbSet<TEntity> _dbEntitySet;
        private bool _disposed;

        public EntityRepository(IApplicationContext context)
        {
            _context = context;
            _dbEntitySet = _context.Set<TEntity>();
        }

        public virtual void Insert(TEntity entity)
        {
            _context.SetAsAdded(entity);
        }

        public virtual void Update(TEntity entity)
        {
            _context.SetAsModified(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            _context.SetAsDeleted(entity);
        }


        public Task<List<TEntity>> GetAllAsync()
        {
            return _dbEntitySet
                .AsNoTracking()
                .ToListAsync();
        }


        public Task<List<TEntity>> GetAllIncludingAsync(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.ToListAsync();
        }

        // БОЛЬШОЙ КОСТЫЛЬ
        //Был сделан во время, когда я не понимал деталей работы линкью и асинкс эвэйт, 
        //сделан для удобства,чтобы иметь возможность сортировать по признаку на сервисе



        public Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet
                .AsNoTracking()
                .FirstOrDefaultAsync(predicate);
        }

        public Task<TEntity> GetSingleIncludingAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.FirstOrDefaultAsync(predicate);
        }

        public Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntitySet
                .AsNoTracking()
                .Where(predicate).ToListAsync();
        }

        public Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            return entities.Where(predicate).ToListAsync();
        }


        private IQueryable<TEntity> FilterQuery(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = IncludeProperties(includeProperties);
            entities = (predicate != null) ? entities.Where(predicate) : entities;
            if (orderBy != null)
            {
                entities = orderBy(entities);
            }
            return entities;
        }
        private IQueryable<TEntity> IncludeProperties(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = _dbEntitySet
                .AsNoTracking()
                ;
            foreach (var includeProperty in includeProperties)
            {
                entities = entities.Include(includeProperty);
            }
            return entities;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
