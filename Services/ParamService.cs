﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Data;
using Core.Services;
using EFModels.Models;

namespace Services
{
    public class ParameterService : IParameterService
    {
        private readonly IRepository<Parameter> _parameterRepository;
        private readonly IRepository<Checkbox> _checkboxRepository;
        private readonly IRepository<Select> _selectRepository;
        private readonly IRepository<Option> _optionRepository;
        private bool _disposed;

        public ParameterService(
            IRepository<Parameter> parameterRepository, 
            IRepository<Checkbox> checkboxRepository, 
            IRepository<Select> selectRepository,
            IRepository<Option> optionRepository)
        {

            _parameterRepository = parameterRepository;
            _checkboxRepository = checkboxRepository;
            _selectRepository = selectRepository;
            _optionRepository = optionRepository;
        }

        public Task<List<Parameter>> GetAllParameters()
        {
            return _parameterRepository.GetAllAsync();
        }
        public Task<List<Parameter>> GetParametersBySubcttegoryId(int subcategoryId)
        {
            return _parameterRepository.FindByAsync(p=>p.SubcategoryId == subcategoryId);
        }
        public Task<List<Parameter>> FindParametersBy(Expression<Func<Parameter, bool>> predicate)
        {
            return _parameterRepository.FindByAsync(predicate);
        }
        public Task<Parameter> GetParameterById(int? id)
        {
            return _parameterRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Parameter parameter)
        {
            if (parameter == null) throw new ArgumentNullException(nameof(parameter));

            var updateCheckboxTask = AddOrUpdateCheckboxGroup(parameter.CheckboxGroup);
            var updateSelectTask = AddOrUpdateSelectGroup(parameter.SelectGroup);
            await updateCheckboxTask;
            await updateSelectTask;
            

            var parameterFromDB = await _parameterRepository.GetSingleIncludingAsync(w => w.Id == parameter.Id);
            if (parameterFromDB == null)
            {
                _parameterRepository.Insert(parameter);
            }
            else
            {
                _parameterRepository.Update(parameter);
            }
            await _parameterRepository.SaveChangesAsync();

            return parameterFromDB?.Id ?? parameter.Id;
        }
        public async Task Remove(int id)
        {
            var parameter = await _parameterRepository.GetSingleAsync(w => w.Id == id);
            _parameterRepository.Delete(parameter);
            await _parameterRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var subcategories = await _parameterRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var parameter in subcategories)
            {
                _parameterRepository.Delete(parameter);
            }

            await _parameterRepository.SaveChangesAsync();
        }

        public async Task AddOrUpdateCheckboxGroup(List<Checkbox> checkboxGroup)
        {
            foreach (var checkbox in checkboxGroup)
            {
                var checkboxFromDb = await _checkboxRepository.GetSingleAsync(w => w.Id == checkbox.Id);
                if (checkboxFromDb == null)
                {
                    _checkboxRepository.Insert(checkbox);
                }
                else
                {
                    _checkboxRepository.Update(checkbox);
                }
            }
        }

        public async Task AddOrUpdateSelectGroup(Select select)
        {

            foreach (var option in select.Options)
            {
                

                var optionFromDb = await _optionRepository.GetSingleIncludingAsync(w => w.Id == option.Id);
                if (optionFromDb == null)
                {
                    _optionRepository.Insert(option);
                }
                else
                {
                    _optionRepository.Update(option);
                }
            }
            var selectFromDb = await _selectRepository.GetSingleIncludingAsync(w => w.Id == select.Id);
            if (selectFromDb == null)
            {
                _selectRepository.Insert(select);
            }
            else
            {
                _selectRepository.Update(select);
            }

        }
    }
}
