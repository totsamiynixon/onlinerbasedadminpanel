﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Data;
using Core.Services;
using EFModels.Models;

namespace Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> _productRepository;
        private bool _disposed;

        public ProductService(IRepository<Product> productRepository)
        {

            _productRepository = productRepository;
        }

        public Task<List<Product>> GetAllProducts()
        {
            return _productRepository.GetAllAsync();
        }
        public Task<List<Product>> FindProductsBy(Expression<Func<Product, bool>> predicate)
        {
            return _productRepository.FindByAsync(predicate);
        }


        public Task<Product> GetProductById(int? id)
        {
            return _productRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Product product)
        {
            if (product == null) throw new ArgumentNullException(nameof(product));

            var productFromDB = await _productRepository.GetSingleIncludingAsync(w => w.Id == product.Id);
            if (productFromDB == null)
            {
                _productRepository.Insert(product);
            }
            else
            {
                _productRepository.Update(product);
            }
            await _productRepository.SaveChangesAsync();

            return productFromDB?.Id ?? product.Id;
        }
        public async Task Remove(int id)
        {
            var product = await _productRepository.GetSingleAsync(w => w.Id == id);
            _productRepository.Delete(product);
            await _productRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var brands = await _productRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var brand in brands)
            {
                _productRepository.Delete(brand);
            }

            await _productRepository.SaveChangesAsync();
        }

    }
}
