﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Data;
using Core.Services;
using EFModels.Models;

namespace Services
{
    public class SubcategoryService : ISubcategoryService
    {
        private readonly IRepository<Subcategory> _subcategoryRepository;
        private bool _disposed;

        public SubcategoryService(IRepository<Subcategory> subcategoryRepository)
        {

            _subcategoryRepository = subcategoryRepository;
        }

        public Task<List<Subcategory>> GetAllSubcategories()
        {
            return _subcategoryRepository.GetAllAsync();
        }

        public Task<Subcategory> GetSubcategoryById(int? id)
        {
            return _subcategoryRepository.GetSingleIncludingAsync(w => w.Id == id);
        }

        public async Task<int> AddOrUpdate(Subcategory subcategory)
        {
            if (subcategory == null) throw new ArgumentNullException(nameof(subcategory));

            var subcategoryFromDB = await _subcategoryRepository.GetSingleIncludingAsync(w => w.Id == subcategory.Id);
            if (subcategoryFromDB == null)
            {
                _subcategoryRepository.Insert(subcategory);
            }
            else
            {
                _subcategoryRepository.Update(subcategory);
            }
            await _subcategoryRepository.SaveChangesAsync();

            return subcategoryFromDB?.Id ?? subcategory.Id;
        }
        public async Task Remove(int id)
        {
            var subcategory = await _subcategoryRepository.GetSingleAsync(w => w.Id == id);
            _subcategoryRepository.Delete(subcategory);
            await _subcategoryRepository.SaveChangesAsync();
        }

        public async Task Remove(List<int> ids)
        {
            if (ids == null) throw new ArgumentNullException(nameof(ids));

            var subcategories = await _subcategoryRepository.FindByAsync(w => ids.Any(a => a == w.Id));

            foreach (var subcategory in subcategories)
            {
                _subcategoryRepository.Delete(subcategory);
            }

            await _subcategoryRepository.SaveChangesAsync();
        }
    }
}
