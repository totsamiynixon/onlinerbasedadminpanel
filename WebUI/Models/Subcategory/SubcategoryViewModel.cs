﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EFModels.Models;

namespace WebUI.Models.Subcategory
{
    public class SubcategoryViewModel
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }

        //public List<Product> Products { get; set; }
    }
}