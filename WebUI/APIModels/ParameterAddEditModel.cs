﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EFModels.Models;

namespace WebUI.APIModels
{
    public class ParameterAddEditModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Checkbox> CheckboxGroup { get; set; }
        public Select SelectGroup { get; set; }
        public bool Countable { get; set; } = false;
        public int SubcategoryId { get; set; }
    }
}