﻿function init(modelInitState) {
    ko.applyBindings(new SubcategoryViewModel(modelInitState));
}

function SubcategoryViewModel(model) {
    var self = this;
    this.Id = model.Id;
    this.Name = ko.observable(model.Name);
    this.Parameters = ko.observableArray([]);
    model.Parameters.forEach(function(parameter, i, arr) {
        self.Parameters.push(new Parameter(parameter));
    });



    //Add parameter block
    this.AddParameterModel = ko.observable(new AddParameterModel(self.id));
    this.ParameterModel = function() {
        var dataJs = ko.mapping.toJS(self.AddParameterModel().Buffer());
        dataJs.SubcategoryId = self.Id;
        dataJs.SelectGroup.AddOption = null;
        dataJs.SelectGroup.RemoveOption = null;
        dataJs.SelectGroup.SetAsRange = null;
        return dataJs;
    }
    this.AddParameter = function () {
        //var dataJS = ko.mapping.toJS(self.AddParameterModel().Buffer());
        ////var dataJson = ko.mapping.toJSON(self.AddParameterModel().Buffer());
        ////var dataJson2 = JSON.parse(self.AddParameterModel().Buffer());
        //dataJS.SelectGroup.AddOption = null;
        //dataJS.SelectGroup.RemoveOption = null;
        //dataJS.SelectGroup.SetAsRange = null;
        $.ajax({
            type: "POST",
            url: "/api/ParameterApi",
            dataType: "application/json",
            success: function(data) {
                alert(data.Message);
                self.Parameters.push(data.Parameter);
                self.AddParameterModel(new AddParameterModel());
            },


            data: self.ParameterModel()
    });
        

    }
    this.currentPage = ko.observable(1);
    this.setCurrentPage = function (page) {
        self.currentPage(page);

    }
}

function Parameter(parameter) {
    if (parameter !== undefined) {
        p: {
            var self = this;
            this.Id = parameter.Id;
            this.Name = ko.observable(parameter.Name);

            this.CheckboxGroup = ko.observableArray([]);
            parameter.CheckboxGroup.forEach(function(checkbox, i, arr) {
                self.CheckboxGroup.push(new Checkbox(checkbox));
            });

            this.SelectGroup = ko.observable(new Select(parameter.SelectGroup));

            this.Countable = ko.observable(parameter.Countable);
            this.SubcategoryId = ko.observable(parameter.SubcategoryId);
        }
    } else {
        p :{ 
            this.Id=0;
            this.Name = ko.observable();

            this.CheckboxGroup = ko.observableArray([]);
            this.SelectGroup = ko.observable(new Select());

            this.Countable = ko.observable(false);
            this.SubcategoryId = ko.observable();
            
        }


    }
    
}

function Checkbox(checkbox) {
    if (checkbox !== undefined) {
        ch: {
            this.Id = checkbox.Id;
            this.Value = ko.observable(checkbox.Value);
            this.Weight = ko.observable(checkbox.Weight);
        }
    }
    else ch: {
        this.Id=0;
        this.Value = ko.observable();
        this.Weight = ko.observable();
    }
}
function Select(select) {
    var self = this;
    if (select !== undefined) {
        s: {
            this.Id = select.Id;
            this.Options = ko.observableArray([]);
            select.Options.forEach(function(option, i, arr) {
                self.Options.push(new Option(option));
            });
            this.HasRange = ko.observable(select.HasRange);
        }
    }
    else s: {
        this.Id = 0;
        this.Options = ko.observableArray([]);      
        this.HasRange = ko.observable(false);
    }
    this.AddOption = function() {
        this.Options.push(new Option());
    }
    this.RemoveOption = function () {
        self.Options.remove(this);
    }
    this.SetAsRange = function() {
        this.HasRange(true);
    }
}


function Option(option) {
    if (option !== undefined) {
        o: {
            this.Id = option.id;
            this.Value = ko.observable(option.Value);
            this.Text = ko.observable(option.Text);
            this.Weight = ko.observable(option.Weight);
        }
    }
    else o: {
        this.Id = 0;
        this.Value = ko.observable();
        this.Text = ko.observable();
        this.Weight = ko.observable();
    }

}

function AddParameterModel() {
    var self = this;
    this.Buffer = ko.observable(new Parameter());

    this.AddCheckbox = function () {
        self.Buffer().CheckboxGroup.push(new Checkbox());
    }
    //Working with checkbox section
    this.HasCheckboxGroup = ko.observable(false);

    //Working with select group
    this.HasSelectGroup = ko.observable(false);

    //Working with Countable
    this.HasCountable = ko.observable(false);

    this.SetCountable = ko.computed(function() {
            self.Buffer().Countable(self.HasCountable());
        },
        this.HasCountable);


    this.ResetCheckboxGroup = function () {
        self.Buffer().CheckboxGroup([]);
        self.HasCheckboxGroup(false);
    }
    this.ResetSelectGroup = function() {
        self.Buffer().SelectGroup(new Select());
        self.HasSelectGroup(false);
    }
    this.ResetCountable = function () {
        this.HasCountable(false);
    }

    this.FullReset = self.Buffer(new Parameter());
    
}


