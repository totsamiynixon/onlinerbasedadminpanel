﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Core.Services;
using Data.Context;
using EFModels.Models;
using WebUI.Infrastructure;
using WebUI.Models.Subcategory;

namespace WebUI.Controllers
{
    public class SubcategoryController : Controller
    {
        private readonly ISubcategoryService _subcategoryService;
        private readonly IParameterService _parameterService;
        public SubcategoryController(
            ISubcategoryService subcategoryService,
            IParameterService parameterService
        )
        {
            _subcategoryService = subcategoryService;
            _parameterService = parameterService;
        }
        // GET: Subcategory
        public async Task<ActionResult> Index()
        {
            //выгружаем из бд подкатегории
            var subcategoriesFromDb = await _subcategoryService.GetAllSubcategories();
            var model = MapperService.ConvertCollection<Subcategory, SubcategoryViewModel>(subcategoriesFromDb);

            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            var model = new SubcategoryAddEditModel();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(SubcategoryAddEditModel model)
        {
            if (ModelState.IsValid)
            {
                var dbSubcategory = MapperService.Convert<SubcategoryAddEditModel, Subcategory>(model);

               var id = await _subcategoryService.AddOrUpdate(dbSubcategory);

                return RedirectToAction("Edit", id);
            }

            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var subcategoryFromDb = await _subcategoryService.GetSubcategoryById(id);
            if (subcategoryFromDb == null) return HttpNotFound();
            else
            {
                var subcategoryModel = MapperService.Convert<Subcategory, SubcategoryAddEditModel>(subcategoryFromDb);
                return View(subcategoryModel);
            }


        }




    }
}