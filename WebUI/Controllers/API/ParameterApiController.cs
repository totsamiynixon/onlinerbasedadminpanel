﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using Core.Services;
using EFModels.Models;
using WebUI.APIModels;
using WebUI.Infrastructure;

namespace WebUI.Controllers.API
{
    public class ParameterApiController : ApiController
    {
        private readonly IParameterService _parameterService;

        public ParameterApiController(
           IParameterService parameterService)
        {
            _parameterService = parameterService;
        }

        [System.Web.Mvc.HttpPost]
        public async Task<JsonDotNetResult> AddOrUpdate(ParameterAddEditModel model)
        {
            //var mappedModel = MapperService.Convert<ParameterAddEditModel, Parameter>(model);

            //await _parameterService.AddOrUpdate(mappedModel);
            //var uploadedParameter = MapperService.Convert<Parameter, ParameterAddEditModel>(mappedModel);
            //var result = new { Parameter = uploadedParameter, Success = "True", Message = "Parameter was uploaded" };
            //return new JsonDotNetResult
            //{
            //    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            //    Data = result
            //};
            return new JsonDotNetResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [System.Web.Mvc.HttpDelete]
        public async Task<JsonDotNetResult> RemoveParameter(int id)
        {
            if (id != 0)
            {
                var parameterFromDb = await _parameterService.GetParameterById(id);
                if (parameterFromDb != null)
                {
                    await _parameterService.Remove(id);
                }
                var result = new {Success = "True", Message = "Parameter was removed from db"};
                return new JsonDotNetResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = result
                };
            }


            else
            {
                var result = new { Success = "False", Message = "There is no parameter with current id" };
                return new JsonDotNetResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = result
                };
            }

        }

    }
}
