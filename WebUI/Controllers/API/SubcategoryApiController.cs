﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Core.Services;
using EFModels.Models;
using WebUI.Infrastructure;
using WebUI.Models.Subcategory;

namespace WebUI.Controllers.API
{
    public class SubcategoryApiController : ApiController
    {
        private readonly ISubcategoryService _subcategoryService;
        public SubcategoryApiController(ISubcategoryService subcategoryService)
        {
            _subcategoryService = subcategoryService;
        }

        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> Update(SubcategoryAddEditModel model)
        {
            if (model.Id != 0)
            {
                var subcategoryFromDb = await _subcategoryService.GetSubcategoryById(model.Id);

                if (subcategoryFromDb != null)
                {
                    var mappedModel = MapperService.Convert<SubcategoryAddEditModel, Subcategory>(model);
                    await _subcategoryService.AddOrUpdate(mappedModel);
                    var result = new {Success = "True", Message = "Update successful"};
                    return new JsonDotNetResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = result
                    };
                }
                else
                {
                    var result = new {Success = "False", Message = "There is no product in database with this Id"};
                    return new JsonDotNetResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = result
                    };
                }
            }
            else
            {
                var result = new { Success = "False", Message = "You are trying to update subcategory with doest exist" };
                return new JsonDotNetResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = result
                };
            };

        }
    }
}
