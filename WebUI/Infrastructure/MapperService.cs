﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EFModels.Models;
using WebUI.APIModels;
using WebUI.Models.Subcategory;

namespace WebUI.Infrastructure
{
    public static class MapperService
    {
        public static void BootStrap()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Subcategory, SubcategoryAddEditModel>();
                cfg.CreateMap<SubcategoryAddEditModel, Subcategory>();
                cfg.CreateMap<Subcategory, SubcategoryViewModel>();
                //cfg.CreateMap<SubcategoryViewModel, Subcategory>();


                cfg.CreateMap<Parameter, ParameterAddEditModel>();
                cfg.CreateMap<ParameterAddEditModel, Parameter>().ForMember(p=>p.Subcategory, opt=>opt.Ignore());

            });
            Mapper.AssertConfigurationIsValid();
        }

        public static TResult Convert<TSource, TResult>(TSource source)
            where TResult : new()
        {
            var target = new TResult();
            Mapper.Map(source, target);

            return target;
        }

        public static List<TResult> ConvertCollection<TSource, TResult>(IList<TSource> sources)
            where TResult : new()
        {
            return sources.Select(Convert<TSource, TResult>).ToList();
        }

        public static void MapToEntity<TSource, TResult>(TSource from, TResult to)
        {
            Mapper.Map(from, to);
        }
    }
}
