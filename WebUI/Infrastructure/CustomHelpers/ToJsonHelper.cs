﻿using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

namespace WebUI.Infrastructure.CustomHelpers
{

        public static class ObjectHelper
        {
            public static IHtmlString ToJson(this HtmlHelper helper, object obj)
            {
                return helper.Raw(
                     JsonConvert.SerializeObject(
                        obj,
                        Formatting.None,
                        new JsonSerializerSettings
                        {
                            PreserveReferencesHandling = PreserveReferencesHandling.None
                        }));
            }
        }
    }

