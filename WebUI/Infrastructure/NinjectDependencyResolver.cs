﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Context;
using Core.Data;
using Core.Services;
using Data.Context;
using Data.Data;
using EFModels.Models;
using Ninject;
using Services;

namespace WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IRepository<Product>>().To<EntityRepository<Product>>();
            kernel.Bind<IRepository<Subcategory>>().To<EntityRepository<Subcategory>>();
            kernel.Bind<IRepository<Parameter>>().To<EntityRepository<Parameter>>();
            kernel.Bind<IRepository<Checkbox>>().To<EntityRepository<Checkbox>>();
                kernel.Bind<IRepository<Option>>().To<EntityRepository<Option>>();
            kernel.Bind<IRepository<Select>>().To<EntityRepository<Select>>();
            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<ISubcategoryService>().To<SubcategoryService>();
            kernel.Bind<IParameterService>().To<ParameterService>();
            kernel.Bind<IApplicationContext>().To<ApplicationContext>();


        }
    }
}