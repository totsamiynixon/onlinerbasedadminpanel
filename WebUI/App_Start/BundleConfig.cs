﻿using System.Web;
using System.Web.Optimization;

namespace WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/StartPackage/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/StartPackage/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/StartPackage/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/StartPackage/bootstrap.js",
                      "~/Scripts/StartPackage/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").IncludeDirectory("~/Scripts/Knockout/", "*.js"));
            bundles.Add(new ScriptBundle("~/bundles/subcategoryExtentions").IncludeDirectory("~/Scripts/SubcategoryExtentions/", "*.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/StartPackage/bootstrap.css",
                      "~/Content/StartPackage/site.css"));
            bundles.Add(new StyleBundle("~/Content/SubcategoryEdit").IncludeDirectory("~/Content/SubcategoryEdit/",
                "*.css"));

        }
    }
}
