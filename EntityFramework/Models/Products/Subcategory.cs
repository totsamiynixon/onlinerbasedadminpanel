﻿using System.Collections.Generic;

namespace EFModels.Models
{
    public class Subcategory : BaseEntity
    {
        public string Name { get; set; }

        public virtual List<Product> Products { get; set; }
        public virtual List<Parameter> Parameters { get; set; }
    }
}