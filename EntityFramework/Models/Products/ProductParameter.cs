﻿namespace EFModels.Models
{
    public class ProductParameter
    {
        public int Id { get; set; }
        public virtual Parameter Parameter { get; set; }

        public int ParameterId { get; set; }

        public int CheckboxId { get; set; }
        public int OptionId { get; set; }

        public int CurrentNumber { get; set; }
    }
}