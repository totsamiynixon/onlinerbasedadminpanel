﻿using System.ComponentModel.DataAnnotations;

namespace EFModels.Models
{
    public class Checkbox: BaseEntity
    {
        [Required]
        public string Value { get; set; }
    }
}