﻿namespace EFModels.Models
{
    public class Option : BaseEntity
    {
        public string Value { get; set; }

        public string Text { get; set; }

        public int Weight { get; set; }
    }
}