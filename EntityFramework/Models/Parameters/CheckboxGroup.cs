﻿using System.Collections.Generic;

namespace EFModels.Models
{
    public class CheckboxGroup
    {
        
        public List<Checkbox> Checkboxes { get; set; }

    }
}