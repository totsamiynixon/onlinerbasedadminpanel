﻿using System.Collections.Generic;

namespace EFModels.Models
{
    public class Select:BaseEntity
    {
        public List<Option> Options { get; set; }
        public bool HasRange { get; set; } = false;
    }
}