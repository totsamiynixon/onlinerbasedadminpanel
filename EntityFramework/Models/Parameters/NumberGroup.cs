﻿namespace EFModels.Models
{
    public class NumberGroup
    {
        public int From { get; set; }
        public int To { get; set; }
    }
}