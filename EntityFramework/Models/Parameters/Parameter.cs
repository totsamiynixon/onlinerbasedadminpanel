﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFModels.Models
{
    public class Parameter : BaseEntity
    {
        public string Name { get; set; }
        public List<Checkbox> CheckboxGroup { get; set; }
        public Select SelectGroup { get; set; }
        public bool Countable { get; set; } = false;
        public virtual Subcategory Subcategory { get; set; }

        public int SubcategoryId { get; set; }
    }
}
