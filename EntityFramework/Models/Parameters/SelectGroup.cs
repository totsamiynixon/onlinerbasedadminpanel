﻿using System.Collections.Generic;

namespace EFModels.Models
{
    public class SelectGroup
    {
        public List<Select> Selects { get; set; }
    }
}