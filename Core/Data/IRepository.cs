﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EFModels.Models;

namespace Core.Data
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseEntity
    {
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        Task<List<TEntity>> GetAllAsync();
        Task<List<TEntity>> GetAllIncludingAsync(params Expression<Func<TEntity, object>>[] includeProperties);

        Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetSingleIncludingAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties);

        Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> FindByAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeProperties);

        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
