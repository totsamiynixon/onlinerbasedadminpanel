﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EFModels.Models;

namespace Core.Services
{
    public interface IParameterService
    {
        Task<List<Parameter>> GetAllParameters();
        Task<List<Parameter>> GetParametersBySubcttegoryId(int subcategoryId);
        Task<List<Parameter>> FindParametersBy(Expression<Func<Parameter, bool>> predicate);
        Task<Parameter> GetParameterById(int? id);
        Task<int> AddOrUpdate(Parameter product);
        Task Remove(int id);

        Task Remove(List<int> ids);
    }
}
