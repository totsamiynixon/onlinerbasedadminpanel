﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EFModels.Models;

namespace Core.Services
{
    public interface IProductService
    {
        Task<List<Product>> GetAllProducts();
        Task<List<Product>> FindProductsBy(Expression<Func<Product, bool>> predicate);
        Task<Product> GetProductById(int? id);
        Task<int> AddOrUpdate(Product product);
        Task Remove(int id);

        Task Remove(List<int> ids);
    }
}
