﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFModels.Models;

namespace Core.Services
{
  public interface ISubcategoryService
    {
        Task<List<Subcategory>> GetAllSubcategories();
        Task<Subcategory> GetSubcategoryById(int? id);

        Task<int> AddOrUpdate(Subcategory subcategory);

        Task Remove(int id);
        Task Remove(List<int> ids);
    }
}
